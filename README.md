# Derco DevOps Test

Implementar servicio que consuma un api rest ya definida, y mostrar los datos a través de navegador (no importa formato).




## Contenido

Este repositorio contiene lo siguiente:

- Aplicación realizada en Django que consume los datos de la API (https://jsonplaceholder.typicode.com/posts)
- Dockerfile que expone la aplicación en el puerto 8000
- Pipeline utilizando branching para la construcción y publicación en el Container Registry de este repositorio 



## Deployment

Para desplegar este código utilizar el siguiente repositorio 

[Derco DevOps Infraestructure](https://gitlab.com/drandredev/derco-infra-test)



## Usabilidad



```bash
  - Crear un issue
  - Crear un Merge Request
  - Realizar un commit a la rama creada
  - Automaticamente el Pipeline contruirá y publicará una imagen de 
    Docker que puede ser utilizada para Staging o proceso de Q&A
  - Al realizar el Merge Request a la rama Main el Pipeline volverá a correr
    el proceso y creará una nueva imagen con el tag :latest para ser deployada
    en el ambiente de producción
```
    

