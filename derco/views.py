import requests, json
from django.shortcuts import render

baseURL = "https://jsonplaceholder.typicode.com/"

def getPosts(request):
    getData = requests.get(baseURL + "posts")
    dataJSON = getData.json()
    return render(request, "derco.html", {'posts' : dataJSON})